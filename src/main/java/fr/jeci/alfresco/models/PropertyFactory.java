package fr.jeci.alfresco.models;

public class PropertyFactory<E extends Model> {
	private Model model;

	public PropertyFactory(Class<E> model) {
		try {
			this.model = model.newInstance();
		}
		catch (InstantiationException | IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public Property text(String name) {
		return new Property(model, name, DataType.TEXT, false);
	}

	public Property mandatoryText(String name) {
		return new Property(model, name, DataType.TEXT, true);
	}

	public Property date(String name) {
		return new Property(model, name, DataType.DATE, false);
	}

	public Property bool(String name) {
		return new Property(model, name, DataType.BOOLEAN, false);
	}

	public Property numFloat(String name) {
		return new Property(model, name, DataType.FLOAT, false);
	}

	public Property numLong(String name) {
		return new Property(model, name, DataType.LONG, false);
	}

	public Property mandatoryLong(String name) {
		return new Property(model, name, DataType.LONG, true);
	}

	public Property integer(String name) {
		return new Property(model, name, DataType.INT, false);
	}

	public Property mandatoryLocale(String name) {
		return new Property(model, name, DataType.LOCALE, true);
	}

	public Property content(String name) {
		return new Property(model, name, DataType.CONTENT, false);
	}

}
