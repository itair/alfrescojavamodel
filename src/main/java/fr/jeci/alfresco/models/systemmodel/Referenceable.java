package fr.jeci.alfresco.models.systemmodel;

import fr.jeci.alfresco.models.Property;

public interface Referenceable {
	public final static Property storeProtocol = SystemModel.pf.mandatoryText("store-protocol");
	public final static Property storeIdentifier = SystemModel.pf.mandatoryText("store-identifier");
	public final static Property nodeUuid = SystemModel.pf.mandatoryText("node-uuid");
	public final static Property nodeDbid = SystemModel.pf.mandatoryLong("node-dbid");
}
