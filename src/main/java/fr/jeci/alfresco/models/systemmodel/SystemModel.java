package fr.jeci.alfresco.models.systemmodel;

import fr.jeci.alfresco.models.AbstractDef;
import fr.jeci.alfresco.models.Model;
import fr.jeci.alfresco.models.PropertyFactory;

public class SystemModel extends Model {

	public static final String NS = "http://www.alfresco.org/model/system/1.0";
	public static final String PREFIX = "sys";
	public static final String NAME = "systemmodel";

	public static PropertyFactory<SystemModel> pf = new PropertyFactory<SystemModel>(SystemModel.class);

	public SystemModel() {
		super(NS, PREFIX, NAME);
	}

	public final static AbstractDef typeDeleted = AbstractDef.newInstance(TypeDeleted.class);
	public final static AbstractDef typeBase = AbstractDef.newInstance(TypeBase.class);

	public static class DefSys extends AbstractDef {
		@Override
		public Model getModel() {
			return Model.newInstance(SystemModel.class);
		}
	}

}
