package fr.jeci.alfresco.models.systemmodel;

import fr.jeci.alfresco.models.systemmodel.SystemModel.DefSys;

public class AspectLocalized extends DefSys implements Localized {
	public static final String NAME = "localized";

	public AspectLocalized() throws InstantiationException, IllegalAccessException {
		this.typeName = NAME;
	}

}
