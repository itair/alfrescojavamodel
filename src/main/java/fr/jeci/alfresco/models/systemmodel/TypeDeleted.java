package fr.jeci.alfresco.models.systemmodel;

import fr.jeci.alfresco.models.Model;
import fr.jeci.alfresco.models.Property;
import fr.jeci.alfresco.models.AbstractDef;

public class TypeDeleted extends AbstractDef {
	public static final String NAME = "deleted";

	public TypeDeleted() throws InstantiationException, IllegalAccessException {
		super.typeName = NAME;
	}

	@Override
	public Model getModel() {
		return Model.newInstance(SystemModel.class);
	}

	public static final Property deleted = SystemModel.pf.numLong("deleted");

}
