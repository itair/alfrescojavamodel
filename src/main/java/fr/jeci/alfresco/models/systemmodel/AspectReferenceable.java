package fr.jeci.alfresco.models.systemmodel;

import fr.jeci.alfresco.models.systemmodel.SystemModel.DefSys;

public class AspectReferenceable extends DefSys implements Referenceable {
	public static final String NAME = "referenceable ";

	public AspectReferenceable() throws InstantiationException, IllegalAccessException {
		this.typeName = NAME;
	}
}
