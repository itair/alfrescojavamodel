package fr.jeci.alfresco.models.systemmodel;

import fr.jeci.alfresco.models.Model;
import fr.jeci.alfresco.models.AbstractDef;

public class TypeBase extends AbstractDef implements Referenceable, Localized {
	public static final String NAME = "base";

	public TypeBase() throws InstantiationException, IllegalAccessException {
		this.typeName = NAME;
	}

	@Override
	public Model getModel() {
		return Model.newInstance(SystemModel.class);
	}

}
