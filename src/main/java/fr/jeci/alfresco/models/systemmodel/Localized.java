package fr.jeci.alfresco.models.systemmodel;

import fr.jeci.alfresco.models.Property;

public interface Localized {
	public final static Property locale = SystemModel.pf.mandatoryLocale("locale");
}
