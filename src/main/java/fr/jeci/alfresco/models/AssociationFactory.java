package fr.jeci.alfresco.models;


public class AssociationFactory<E extends Model> {
	private Model model;

	public AssociationFactory(Class<E> model) {
		try {
			this.model = model.newInstance();
		}
		catch (InstantiationException | IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public ChildAssociation childAssoc(String name, AbstractDef source, AbstractDef target, String card) {
		return new ChildAssociation(model, name, source, target, Byte.parseByte(card, 2));
	}

}
