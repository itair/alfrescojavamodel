package fr.jeci.alfresco.models;

import org.alfresco.service.namespace.QName;

/**
 * Représentation d'une Proriété dans Alfresco
 * 
 * @author jlesage
 */
public class Property {
	private final Model model;
	private final String name;
	private final DataType type;
	private final boolean mandatory;

	public Property(Model model, String name, DataType type, boolean mandatory) {
		this.model = model;
		this.name = name;
		this.type = type;
		this.mandatory = mandatory;
	}

	public String getName() {
		return name;
	}

	public DataType getType() {
		return type;
	}

	public boolean isMandatory() {
		return mandatory;
	}

	public String shortQname() {
		return this.model.getPrefix() + ":" + getName();
	}

	public String fullQname() {
		return "{" + this.model.getNs() + "}" + getName();
	}

	public QName qname() {
		return QName.createQName(this.model.getNs(), getName());
	}

	@Override
	public String toString() {
		return shortQname();
	}
}
