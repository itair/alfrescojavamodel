package fr.jeci.alfresco.models;

import org.alfresco.service.namespace.QName;

public abstract class AbstractDef {
	protected String typeName;

	public String getName() {
		return typeName;
	}

	public String shortQname() {
		return getModel().getPrefix() + ":" + getName();
	}

	public String fullQname() {
		return "{" + getModel().getNs() + "}" + getName();
	}

	public QName qname() {
		return QName.createQName(getModel().getNs(), getName());
	}

	public static AbstractDef newInstance(Class<? extends AbstractDef> type) {
		try {
			return type.newInstance();
		}
		catch (InstantiationException | IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	public abstract Model getModel();

	
	@Override
	public String toString() {
		return this.shortQname();
	}
}
