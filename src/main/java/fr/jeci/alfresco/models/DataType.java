package fr.jeci.alfresco.models;

/**
 * Représentation d'un type définit dans le DitiocnayModel d'Alfresco
 * 
 * @author jlesage
 */
public enum DataType {
	ANY,
	ENCRYPTED,
	TEXT,
	MLTEXT,
	CONTENT,
	INT,
	LONG,
	FLOAT,
	DOUBLE,
	DATE,
	DATETIME,
	BOOLEAN,
	QNAME,
	NODEREF,
	CHILDASSOCREF,
	ASSOCREF,
	PATH,
	CATEGORY,
	LOCALE,
	VERSION,
	PERIOD;

	public String qname() {
		return "d:" + name().toLowerCase();
	}
}
