package fr.jeci.alfresco.models;

public class ChildAssociation {
	private final Model model;
	private final String name;
	private final AbstractDef source;
	private final AbstractDef target;
	private final byte cardinalite;

	public ChildAssociation(Model model, String name, AbstractDef source, AbstractDef target, byte cardinalite) {
		this.model = model;
		this.name = name;
		this.source = source;
		this.target = target;
		this.cardinalite = cardinalite;
	}

	public static void main(String[] args) {
		System.out.println(Byte.parseByte("0101", 2));
	}

	public String getName() {
		return name;
	}

	public AbstractDef getSource() {
		return source;
	}

	public AbstractDef getTarget() {
		return target;
	}

	public byte getCardinalite() {
		return cardinalite;
	}

	public String shortQname() {
		return model.getPrefix() + ":" + getName();
	}

	public String fullQname() {
		return "{" + model.getNs() + "}" + getName();
	}
}
