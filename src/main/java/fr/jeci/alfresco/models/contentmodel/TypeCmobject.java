package fr.jeci.alfresco.models.contentmodel;

import fr.jeci.alfresco.models.Model;
import fr.jeci.alfresco.models.Property;
import fr.jeci.alfresco.models.systemmodel.TypeBase;

public class TypeCmobject extends TypeBase implements Auditable {
	public static final String NAME = "base";

	public TypeCmobject() throws InstantiationException, IllegalAccessException {
		this.typeName = NAME;
	}

	@Override
	public Model getModel() {
		return Model.newInstance(ContentModel.class);
	}

	public static final Property name = ContentModel.pf.mandatoryText("name");

}
