package fr.jeci.alfresco.models.contentmodel;

import fr.jeci.alfresco.models.ChildAssociation;
import fr.jeci.alfresco.models.systemmodel.SystemModel;

public class TypeFolder extends TypeCmobject {
	public static final String NAME = "folder";

	public TypeFolder() throws InstantiationException, IllegalAccessException {
		this.typeName = NAME;
	}

	public static final ChildAssociation contains = ContentModel.af.childAssoc("contains", null, SystemModel.typeBase,
			"0101");
}
