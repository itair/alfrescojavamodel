package fr.jeci.alfresco.models.contentmodel;

import fr.jeci.alfresco.models.contentmodel.ContentModel.DefCm;

public class AspectAuditable extends DefCm implements Auditable {
	public static final String NAME = "auditable";

	public AspectAuditable() throws InstantiationException, IllegalAccessException {
		this.typeName = NAME;
	}
}
