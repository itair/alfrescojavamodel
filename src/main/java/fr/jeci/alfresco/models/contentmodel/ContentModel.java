package fr.jeci.alfresco.models.contentmodel;

import fr.jeci.alfresco.models.AssociationFactory;
import fr.jeci.alfresco.models.Model;
import fr.jeci.alfresco.models.PropertyFactory;
import fr.jeci.alfresco.models.AbstractDef;

public class ContentModel extends Model {

	public static final String NS = "http://www.alfresco.org/model/content/1.0";
	public static final String PREFIX = "cm";
	public static final String NAME = "contentmodel";

	public static PropertyFactory<ContentModel> pf = new PropertyFactory<ContentModel>(ContentModel.class);
	public static AssociationFactory<ContentModel> af = new AssociationFactory<ContentModel>(ContentModel.class);

	public ContentModel() {
		super(NS, PREFIX, NAME);
	}

	public final static AbstractDef typeCmobject = AbstractDef.newInstance(TypeCmobject.class);
	public final static AbstractDef typeFolder = AbstractDef.newInstance(TypeFolder.class);
	public final static AbstractDef typeContent = AbstractDef.newInstance(TypeContent.class);

	public static class DefCm extends AbstractDef {
		@Override
		public Model getModel() {
			return Model.newInstance(ContentModel.class);
		}
	}
}
