package fr.jeci.alfresco.models.contentmodel;

import fr.jeci.alfresco.models.Property;

public class TypeContent extends TypeCmobject {
	public static final String NAME = "content";

	public TypeContent() throws InstantiationException, IllegalAccessException {
		super.typeName = NAME;
	}

	public static final Property content = ContentModel.pf.content("content");

}
