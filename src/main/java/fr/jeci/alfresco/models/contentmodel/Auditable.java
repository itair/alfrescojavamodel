package fr.jeci.alfresco.models.contentmodel;

import fr.jeci.alfresco.models.Property;

public interface Auditable {
	public final static Property created = ContentModel.pf.mandatoryText("created");
	public final static Property creator = ContentModel.pf.mandatoryText("creator");
	public final static Property modified = ContentModel.pf.mandatoryText("modified");
	public final static Property modifier = ContentModel.pf.mandatoryText("modifier");
	public final static Property accessed = ContentModel.pf.mandatoryText("accessed");

}
