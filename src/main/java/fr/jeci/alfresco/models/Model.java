package fr.jeci.alfresco.models;


public abstract class Model {
	private final String nameSpace;
	private final String prefix;
	private final String name;

	public Model(String nameSpace, String prefix, String name) {
		this.nameSpace = nameSpace;
		this.prefix = prefix;
		this.name = name;
	}

	public String getNameSpace() {
		return nameSpace;
	}

	public String getNs() {
		return nameSpace;
	}

	public String getPrefix() {
		return prefix;
	}

	public String getName() {
		return name;
	}

	public static Model newInstance(Class<? extends Model> model) {
		// TODO CACHE ?
		try {
			return model.newInstance();
		}
		catch (InstantiationException | IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
}
